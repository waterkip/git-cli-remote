use strict;
use warnings;
use Test::More 0.96;
use Test::Exception;
use Sub::Override;

use_ok("Git::CLI::Remote");

my $override = Sub::Override->new(
    'Module::Pluggable::Object::plugins' => sub {
        return 'Git::CLI::Remote::Provider::Test'
    }
);

my $found = Git::CLI::Remote->has_provider('none');
ok($found, "We have a 'none' provider");

$found = Git::CLI::Remote->has_provider('gitlab');
ok(!$found, ".. and not 'gitlab'");

my $provider = Git::CLI::Remote->get_provider('none');
isa_ok(
    $provider,
    "Git::CLI::Remote::Provider::Test",
    "Found our 'none' provider"
);

throws_ok(
    sub {
        Git::CLI::Remote->get_provider('gitlab');
    },
    qr/Unable to find provider plugin for gitlab/,
    ".. and not 'gitlab'",
);

throws_ok(
    sub {
        Git::CLI::Remote->get_provider();
    },
    qr/No provider given!/,
    ".. and dies when no provider is provided",
);



BEGIN {

    package Git::CLI::Remote::Provider::Test {
        use Moo;
        sub provider_name {
            "none";
        }

        1;
    };


}

done_testing;
