use utf8;
package Git::CLI::Remote;
our $VERSION = '0.001';
use Moo;
use namespace::autoclean;
use Carp qw(croak);

use Module::Pluggable::Object;
use List::Util qw(first);

# ABSTRACT: Manage Git remotes via the CLI

{
    my @PLUGINS;
    sub _get_all_providers {
        my $self = shift;

        return @PLUGINS if @PLUGINS;

        my $search_path = 'Git::CLI::Remote::Provider';

        my $finder = Module::Pluggable::Object->new(
            search_path => $search_path,
            require     => 1,
        );

        @PLUGINS = $finder->plugins();
        return @PLUGINS;

    }
}

sub has_provider {
    my ($self, $provider) = @_;

    return first { $_->provider_name eq $provider }
        $self->_get_all_providers();
    return;
}

sub get_provider {
    my ($self, $provider, %opts) = @_;

    croak "No provider given!" unless $provider;

    my $plugin = $self->has_provider($provider);

    croak "Unable to find provider plugin for $provider" unless $plugin;

    return $plugin->new(%opts);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 DESCRIPTION

This module is a CLI solution to create, update and delete pull and/or merge
requests

=head1 SYNOPSIS

    use Git::CLI::Remote;

    my %options = (
        # TODO: define good options for each provider
    );

    my $provider = Git::CLI::Remote->get_provider('gitlab', %options);

    my $branch = 'foo';

    # $provider is an instance of Git::CLI::Remote::Provider::Gitlab
    $provider->create_mr($branch);
    $provider->update_mr($branch);


=head1 ATTRIBUTES

=head1 METHODS

=head2 has_provider

Get a provider, dies when the provider plugin cannot be found. You can use this
if you want to instantiate a provider yourself.

=head2 get_provider

    my $provider = Git::CLI::Remote->get_provider('gitlab', %options);

Instantiate a provider plugin. Optionally pass options to the instantiation of
the module

=cut
