# DESCRIPTION

This module is a CLI solution to create, update and delete pull and/or merge
requests

# SYNOPSIS

    use Git::CLI::Remote;

    my %options = (
        # TODO: define good options for each provider
    );

    my $provider = Git::CLI::Remote->get_provider('gitlab', %options);

    my $branch = 'foo';

    # $provider is an instance of Git::CLI::Remote::Provider::Gitlab
    $provider->create_mr($branch);
    $provider->update_mr($branch);

# ATTRIBUTES

# METHODS

## has\_provider

Get a provider, dies when the provider plugin cannot be found. You can use this
if you want to instantiate a provider yourself.

## get\_provider

    my $provider = Git::CLI::Remote->get_provider('gitlab', %options);

Instantiate a provider plugin. Optionally pass options to the instantiation of
the module
